import locale

#İki çeşit menü itemi oluşturulabilir komut çalıştıran veya alt menülere açılan bir menu
#komut parametersine çalıştırılmasını istediğiniz komutu yazınız.
#Bir fonksiyon yazıp onu çalırmak istiyorsanuz komut parametresini "fonk:fonk_adi,parametre,parametre"
#şeklinde kullanabilirsiniz
#Eğer komutunuzda entryde yazanı kullanmak istiyorsanız komut parametresine {arama} ekleyin
#Metin menüde görüntülencek olan yazıdır boş geçmeyiniz
#Simge sizden bir png dosyasının adresini bekler
#Simge kullanmak zorunda değilsiniz None ile geçiştirebilirsiniz
#Genel menü oluşturma mantığı aşağıda verilmiştir. boşlukları dilediğiniz gibi kullanabilrsiniz
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Tüm veriler string olmalıdır ve tırnak içine yazılmalıdır listede 3 item olmalıdır
#Parametreleri boş bırakmak yerine "None" yazını "tırkantk içinde"
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


alt_menu_2 = [("Madde_3",          "ornek komut",           "None"),
			  ("Madde_4",          "ornek komut",        "None"),]

alt_menu_1 = [("Alt_Menu_2",      alt_menu_2,           "./simgeler/menu.png"),
			  ("Madde_1",        "shutdown -r now",        "None"),]


#           (metin,           komut,                       simge) 
menu_ayar = [("Fonk_Or1",        "fonk:calis",             "./simgeler/menu.png"),
			("Fonk_Or2",         "fonk:cikti_ver,selam",   "./simgeler/menu.png"),
			("Fonk_Or3",         "fonk:cikti_ver,{arama}", "./simgeler/menu.png"),
			("Alt_Menu_1",       alt_menu_1,              "./simgeler/menu.png"),
			("İndir",            "wget {arama}",           "./simgeler/indir.png"),
			("Sistem Bilgi",     "htop",                   "./simgeler/s_bilgi.png"),
			("Yeniden Başlat",   "shutdown -r now",        "./simgeler/y_baslat.png"),
			("Kapat",            "shutdown now",           "./simgeler/kapat.png"),]

#iconlar png olmalı ve boyutu 32x32 olmalı
#Alt dizinler aranmaz lütfen iconların olduğu dizini yazınız.
icon_yolu = "/usr/share/icons/hicolor/32x32/apps/"

#Dil menü açıklamaları için gerekli
dil = locale.getdefaultlocale()[0].split("_")[0]

#uygulama dizinleri .desktop dosyası aramasını istediğimiz dizinler
uygulama_dizinler = ["/usr/share/applications/"]
#Sistemdeki komutlar arasında arama yapmak için dizin ismi
komut_dizinler = ["/usr/bin/"]
#log dosyası konumu
log_dosya = "./komut.log"

renkler = {"ap":"#272822",
			"ho":"#a7e22e",
			"te":"#cfd0c2",
			"se":"#48483e"}
#Genel font düzenine göre simgeler ve menüler şekillenmektedir
menu_font = ("Noto Sans",12)
#Listede görünecek en fazla madde sayısı
liste_madde_sayisi = 5

#Bir menü öğesine tıklanınca tk menü kapansın mı
men_tik_kapat = True
#Listede bulunan desktop öğesine tıklanınca yada enter basılınca liste kapansın mı
desk_tik_kapat = True
#Listede bulunan log öğesine tıklanınca yada enter basılınca liste kapansın mı
log_tik_kapat = True

def calis():
	print("Çalıştım")

def cikti_ver(cikti):
	print(cikti)

def arama_soyle(gelen):
	print(gelen)
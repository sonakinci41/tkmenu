# TkMenu
Pythonun varsayılan kütüphaneleri kullanılarak oluşturulan basit ve işlevsel linux menü aracı

## Nasıl Çalıştırırım?
```
python3 tkmenu
```

## Nasıl Düzenlerim
Bütün ayarlar ayar.py içerisinde yer almaktadır.

### Menü Ayarları
İlk görüntülenecek olan menü menu_ayar değişkenine atanmalı ve "list" biçiminde olmalı.
liste içerisindeki her bir satır için bir "tuple" yazacağız ve "tuple" 3 elemanlı olamalı.
1. eleman : Satırda görüntülenecek metini yazacağız. "str" biçiminde olmalı ve boş bıraklamamalı.
2. eleman : Satıra tıklandığında çalışacak komutu yazacağız. 3 farklı komut tipi kullanabiliriz.
    * standart komut : subprocess ile çağırılacak olan bu komut bir sistem komutu olabilir "str" olmalı. Komut içerisinde menü arama kısmında belirtilen yazıyı kullanmak için {arama} ifadesini kullanabiliriz.
    * alt menu : Menümüzün alt menüleri olsun istiyorsak bu bölüme bir değişken adı vermeliyiz ve değişken tıpkı menümüz gibi "list" olmalı. Bu alt menüyü atadğımız değişkeni menu_ayar değişkeninden önce tanımlamayı unutmayınız
    * fonksiyon : ayar.py içersinde yazdığınız fonksiyonları menü üzerinden çağırabilirsiniz. Bunun için "str" formatında yazacağımız ifade içine "fonk:fonksiyon_adi,parametre1,parametre2..." şeklinde devam ederek parametrelerimizi girebiliriz. "fonk:" ifadesi bir fonksiyon olduğunun anlaşılması içindir. fonksiyon_adi yerine tanımladığınız fonksiyonun adını yazmalıyız. Her hangi bir parametre yazmak istemiyorsanız devam etmeyebilirsiniz. Parametreler arasında menüde aranan ifadeyi kullanmak için parametreye {arama} ifadesini ekleyebilirsiniz.
3. eleman : Satırda gösterilmesi istenen simgenin tam yolunu "str" biçiminde yazmalıyız. Gözükmesini istediğimiz simge png veya gif olmalı. Şimdilik jpg ve svg dosyaları desteği yoktur. Ayrıca simge gösterilmesini istemiyorsanız "None" ifadesini yazabilirsiniz. İfadenin "str" biçiminde olması gerektiğini unutmayınız.

Örnek:
```
menu_ayar = [("Fonk_Or1",        "fonk:calis",             "./simgeler/menu.png"),
			("Fonk_Or2",         "fonk:cikti_ver,selam",   "./simgeler/menu.png"),
			("Fonk_Or3",         "fonk:cikti_ver,{arama}", "./simgeler/menu.png"),
			("Alt_Menu_1",       alt_menu_1,              "./simgeler/menu.png"),
			("İndir",            "wget {arama}",           "./simgeler/indir.png"),
			("Sistem Bilgi",     "htop",                   "./simgeler/s_bilgi.png"),
			("Yeniden Başlat",   "shutdown -r now",        "./simgeler/y_baslat.png"),
			("Kapat",            "shutdown now",           "./simgeler/kapat.png"),]
```

Alt menüler içinde aynı kurallar geçerlidir. Keyfinize göre menüleri dallandırabilirsiniz.

### Görünüm Ayarları
* icon_yolu değişkeni .desktop dosyalarının iconlarının aranacağı dizindir "str" biçeminde yazacağız. Yazdığımız adresin içerisinde png dosyaları olmaldır. Aynı zamanda desktop dosyaları çoğunluklar bir app olduğundan işaret ettiğimiz dizinin apps dizini olduması gerektiğini unutmayalım.

Örnek:
```
icon_yolu = "/usr/share/icons/hicolor/32x32/apps/"
```

*renkler değişkeni uygulamamızda kullanılan renkleri belirlediğimiz "dict" dir. 4 parametre bekler.
1 "ap" arkaplan rengi olarak kullanılacak olan renktir
2 "ho" arama barının etrafında bulunan çerçevenin rengidir
3 "te" yazıların rengidir
4 "se" fare ve klavye hareketi sonucu üzerine gelen öğeyi belirtmek için kullanılan renktir.

Renkler Hex Color Code formatında olmalıdır
```
renkler = {"ap":"#272822",
			"ho":"#a7e22e",
			"te":"#cfd0c2",
			"se":"#48483e"}
```

*menu_font değişkeni uygulamada görüntülenecek fontun adını ve boyutunu bekler Font adı "str" boyutu "int" olmalıdır. Font sadece text boyutunu değil menu ve listelerin boyutunuda etkileyecektir. Font boyutunu çok arttırmanız veya azaltmanız durumunda görünümde bozulmalar yaşana bilir.

Örnek:
```
menu_font = ("Noto Sans",12)
```

*liste_madde_sayisi değişkeni arama bölümünün alt kısmında gözüken listede en fazla kaç madde gözükeceğini ayarlar. Çok fazla madde görmek isterseniz ekran boyutunuzu aşabilirsiniz.

Örnek
```
liste_madde_sayisi = 5
```
### Dil Ayarları
Dil ayarları desktop açıklamaları için dil ayarlarının belirlenmesi gerekir. Eğer ayarı değiştirmezseniz otomatik dili algılayacaktır. Değiştirmek istediğiniz taktirde "str" formatında dilinizi belirtmelisiniz.

Varsayılan ayar
```
dil = locale.getdefaultlocale()[0].split("_")[0]
```

### Diğer Ayarlar
* uygulama dizinleri değişkeni uygulamaların arancağı dizinleri bekler "list" biçiminde yazmalıyız. .desktop dosyalarınızın bulundukları dizinleri ekleyebilirsiniz

Örnek
```
uygulama_dizinler = ["/usr/share/applications/"]
```

*komut dizinleri değişkeni sistemde heryerden erişilebilir komutların çağırıldığı dizinleri bekler "list" biçiminde yazmalıyız.

Örnek:
```
komut_dizinler = ["/usr/bin/"]
```

* log_dosya değişkeni desktop ve genel komutlar dışında verdiğimiz komutları sakladığımız bir log dosyası tutulur. Bu log dosyasındaki komutları sol ve sag ok ile dolaşa biliriz. Komutların yazılacağı dosyanın yolunu bekler "str" biçiminde olmalıdır.

Örnek:
```
log_dosya = "./komut.log"
```

* menu_tik_kapat desk_tik_kapat log_tik_kapat dosyaları tkmenu de yapılan işlemden sonra tkmenü kapatılsın mı kapatılmasın mı belirtitiğimiz kısımdır.
- Menudeki tıklamalarımız ardından tkmenu kapatılsın istersek menu_tik_kapat True olmalı aksi halde False olamalı
- Listeden bir desktop dosyası yada global bir komut açlıştırıldığında tkmenu kapansın istiyorsak desk_tik_kapat True olmalı aksi halde False olmalı
- Eğer log tutulan komutlardan biri çalıştırıldığında tkmenu kapatılsın istiyorsak log_tik_kapat True olmalı aksi halde False olmalı

Örnek:
```
men_tik_kapat = True
desk_tik_kapat = True
log_tik_kapat = True
```

### Tuş hatırlatmaları
1 - Eski verilmiş komutlarınız arasında dolaşmak için < sol ok ve > sağ ok tuşlarını kullanabilirsiniz

2 - Menüyü kapatmak için ESC tuşunu kullanabilirsiniz.